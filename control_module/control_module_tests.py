from control_module import ControlModule
import time

def blink_leds():
	"""
	Blinks each LED for the relay box.
	"""
	cm = ControlModule()
	cm.setup_gpio()
	print("Turning lights off.")
	# cm.turn_off_outlets()
	time.sleep(1)
	print("Running on/off test.")
	for i in range(1, 9):
		print("Toggling outlet {}.".format(i))
		# cm.toggle_outlet(i)
		time.sleep(0.5)
	print("on/off test complete.")
	time.sleep(1)
	print("Turning off outlets.")
	# cm.turn_off_outlets()
	time.sleep(1)
	print("Running PWM/flashing test.")
	for i in range(1, 9):
		print("Flashing outlet {} at {}Hz.".format(i, i))
		# cm.flash_outlet(i, i)
		time.sleep(1)
	print("Flashing test complete.")
	time.sleep(1)
	print("Turning off outlets.")
	time.sleep(1)
	# cm.turn_off_outlets()
	time.sleep(1)
	print("Cleaning up.")
	time.sleep(1)
	cm.cleanup()
	return True

def test_rotary_encoder():
	"""
	While loop while print out callback values for 
	the rotary's push button and dial.
	"""
	cm = ControlModule()
	cm.setup_gpio()
	#cm.run_rotary_pb_test()
	while True:
		time.sleep(0.01)


if __name__ == '__main__':
	cm = ControlModule()
	try:
		#blink_leds()
		test_rotary_encoder()
	except (KeyboardInterrupt, SystemExit):
		print("Exception occurred, cleaning up GPIO.")
		cm.cleanup()
