#!/usr/bin/env python
# import rospy
import redis
import time
import json
import os
import sys
from control_module import ControlModule

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

print(PROJECT_ROOT)

# Loads environment based on deployment location:
# sys.path.insert(1, PROJECT_ROOT)
sys.path.insert(1, os.path.join(PROJECT_ROOT, ".."))

from environment import load_env

print("Loading env")
load_env()
print("{}".format(os.environ))


class ControlModuleNode:

	def __init__(self):

		self.control_module = ControlModule()  # control_module controller instance

		self.redis = redis.Redis(host=os.environ.get('REDIS_HOST', 'localhost'), port=os.environ.get('REDIS_PORT', 6379), db=0)
		self.pubsub = self.redis.pubsub()
		self.sub_thread = None

		# Subscribers:
		self.relay_states_sub = "relay-states-pub"
		self.relay_freqs_sub = "relay-freqs-pub"

		# Publishers:
		self.selected_outlet_pub = "control-module-selected-outlet"  # control module node channel
		self.control_mode_pub = "control-module-control-mode"  # control module node channel
		self.curr_freq_pub = "control-module-curr-freq"  # control module node channel
		self.turn_off_outlet_pub = "relay-off-sub"  # relay node channel
		self.toggle_outlet_pub = "relay-toggle-sub"  # relay node channel
		self.flash_outlet_pub = "relay-flash-sub"  # relay node channel

	def init_sub(self):
		self.pubsub.psubscribe(**{self.relay_states_sub: self.relay_states_callback})
		# self.pubsub.psubscribe(**{self.relay_freqs_sub: self.relay_freqs_callback})  # CM doesn't need freq ch., but a UI would
		self.sub_thread = self.pubsub.run_in_thread(sleep_time=0.001)  # runs sub in separate thread (thread.stop() to kill)

	def relay_states_callback(self, message):
		"""
		Gets updated relay states.
		"""
		# self.control_module.relay_states = json.loads(message['data'])
		relay_states = json.loads(message['data'])

		# TODO: Update LEDs to correspond with outlet toggle/flashing
		self.control_module.update_leds(relay_states)


	# def relay_freqs_callback(self, message):
	# 	"""
	# 	Gets updated relay freqs.
	# 	"""
	# 	# self.control_module.relay_freqs = json.loads(message['data'])
	# 	pass

	def stop_node(self):
		if self.sub_thread:
			self.sub_thread.stop()  # stops sub thread
		self.control_module.cleanup()  # cleans up GPIO

	def start_node(self):
		self.control_module.redis = self.redis
		self.control_module.turn_off_outlet_pub = self.turn_off_outlet_pub
		self.control_module.toggle_outlet_pub = self.toggle_outlet_pub
		self.control_module.flash_outlet_pub = self.flash_outlet_pub
		self.control_module.selected_outlet_pub = self.selected_outlet_pub
		self.control_module.control_mode_pub = self.control_mode_pub
		self.control_module.curr_freq_pub = self.curr_freq_pub
		self.control_module.main()  # RC startup (sets up GPIO)
		self.init_sub()  # starts sub thread



if __name__ == '__main__':

	control_module_node = ControlModuleNode()

	try:

		print("Initiating control_module_node.")
		print("Running control_module_node start_node function.")

		control_module_node.start_node()

		while True:
			time.sleep(0.01)

	except Exception as e:
		print("Exception occurred: {}".format(e))
		print("Shutting down control_module_node controls.")
		control_module_node.stop_node()
