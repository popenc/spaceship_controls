import RPi.GPIO as GPIO
from time import sleep
import os
import sys
import json
import redis

# root_path = os.path.abspath(os.path.split(__file__)[0])
# sys.path.insert(0, os.path.join(root_path, '..'))

# from relay_controls.relay_controller import RelayController



class ControlModule:
	"""
	Initial design has 8 push buttons with correspnding LEDs,
	and a rotary encoder for going between on/off and flash modes,
	where the dial controls the flashing speed (the inputs).
	"""

	def __init__(self):

		# self.relay_controller = RelayController()

		self.default_freq = 1.0  # 1Hz default freq for outlets (flash mode)
		
		self.pb_pins = [29, 31, 33, 35, 37, 36, 38, 40]  # pb 1 - 8 pins (inputs)
		self.led_pins = [3, 5, 7, 11, 13, 15, 19, 21]  # pb leds 1 - 8 (outputs)

		# Defines rotary encoder pins:
		self.rot_clk_pin = 12
		self.rot_dt_pin = 16
		self.rot_sw_pin = 18
		self.mode_led_pin = 23  # led to indicate control mode (e.g., toggle, flash)

		self.mode_pwm = None

		# Keeps track of rotary encoder position:
		self.last_state = 0
		# self.curr_state = 0
		self.curr_freq = None
		self.min_freq = 0.1
		self.max_freq = 10.0
		self.counter = 0
		self.freq_step = 0.5  # 
		self.count_increment = 1  # for every n counts, go up/down freq by freq_step

		# Control mode settings:
		self.control_modes = ['toggle', 'flash']  # on/off and flashing modes controlled by rotary button
		self.control_mode = 'toggle'  # default is on/off mode
		self.control_press_count = 0  # keeps track of rotary pb callback being triggered 2x per press

		self.selected_outlet = None  # selected outlet to change freq with dial

		# Redis configuration:
		self.redis = None  # redis instance used to publish (from relay_node.py, where sub is located)
		self.turn_off_outlet_pub = None
		self.toggle_outlet_pub = None
		self.flash_outlet_pub = None
		self.control_mode_pub = None
		self.selected_outlet_pub = None
		self.curr_freq_pub = None
		# self.relay_freqs = None  # relay_freqs from relay_node
		self.relay_freqs = [None, None, None, None, None, None, None]
		self.relay_states = None  # relay_states from relay_node

	def setup_gpio(self):
		"""
		Sets up RPi GPIO pins for push buttons and
		rotary dial of the CI.
		"""
		GPIO.setmode(GPIO.BOARD)  # sets GPIO mode to use pin numbers
		# Sets up push buttons as input, rising edge detection:
		for _pin in self.pb_pins:
			GPIO.setup(_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
			GPIO.add_event_detect(_pin, GPIO.RISING, callback=self.pb_press_callback, bouncetime=100)
		# Sets up CLK, DT, and SW pins of rotary encoder, rising edge detection:
		GPIO.setup(self.rot_clk_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.add_event_detect(self.rot_clk_pin, GPIO.RISING, callback=self.rot_clK_callback, bouncetime=300)
		GPIO.setup(self.rot_dt_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.setup(self.rot_sw_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
		GPIO.add_event_detect(self.rot_sw_pin, GPIO.FALLING, callback=self.rot_sw_callback, bouncetime=100)
		# Sets up LED mode indicator:
		GPIO.setup(self.mode_led_pin, GPIO.OUT)
		GPIO.output(self.mode_led_pin, GPIO.HIGH)  # turn on as intial/default state when CI is on

		# TODO: Run modules separately using redis to talk.
		# # Sets up relay controller pins:
		# self.relay_controller.setup_gpio()  # sets relay pins as PWM
		# self.relay_controller.turn_off_outlets()  # turns outlets off (100% DC)

		self.last_state = GPIO.input(self.rot_clk_pin)

	def setup_leds_gpio(self):
		"""
		Sets up RPi GPIO pins.
		"""
		GPIO.setmode(GPIO.BOARD)
		led_index = 0
		for led_pin in self.led_pins:
			print("Setting up relay pin {} as output.".format(led_pin))
			GPIO.setup(led_pin, GPIO.OUT)  # sets as OUTPUT prior to PWM instance
			GPIO.output(led_pin, GPIO.LOW)
			led_index += 1

	def update_leds(self, relay_states):
		"""
		Updates LEDs based on relay node channel.
		"""
		led_index = 0
		for relay in relay_states:
			if relay == 'on':
				print("Turning LED on.")
				GPIO.output(self.led_pins[led_index], GPIO.HIGH)
			elif relay == 'off':
				print("Turning LED off.")
				GPIO.output(self.led_pins[led_index], GPIO.LOW)
			led_index += 1
		print("\n")

	def is_valid_freq(self, freq):
		"""
		Error checking for frequency.
		"""
		try:
			freq = float(freq)
		except Exception as e:
			print("Couldn't convert freq to float: {}".format(e))
		if freq < self.min_freq or freq > self.max_freq:
			return False
		return True

	def handle_freq_change(self, inc_dir):
		"""
		Handles selected outlet's frequency change from rotary dial.
		"""
		if not self.control_mode == 'flash' or not self.selected_outlet:
			return False
		# if not self.is_valid_outlet_number(self.selected_outlet):
		# 	return False
		# Changes selected outlet freq if in flash mode
		# self.curr_freq = self.relay_controller.relay_freqs[self.selected_outlet - 1]

		self.curr_freq = self.relay_freqs[self.selected_outlet - 1]
		self.redis.publish(self.curr_freq_pub, json.dumps(self.curr_freq))

		print("Initial freqs: {}".format(self.relay_freqs))
		print("Current frequency of selected outlet {}: {}".format(self.selected_outlet, self.curr_freq))

		orig_counter = self.counter
		if inc_dir == 'decrement':
			new_freq = self.curr_freq - self.freq_step
			self.counter += 1
		elif inc_dir == 'increment':
			new_freq = self.curr_freq + self.freq_step
			self.counter -= 1
		if not self.is_valid_freq(new_freq):
			# TODO: Call is_valid_freq through API?
			print("Invalid freq value: {}".format(new_freq))
			self.counter = orig_counter
			return
			# new_freq = self.curr_freq
			# # Resets counter if invalid freq:
			# if inc_dir == 'decrement':
			# 	self.counter += 1
			# elif inc_dir == 'increment':
			# 	self.counter -= 1

		print("New frequency : {}".format(new_freq))
		
		self.relay_freqs[self.selected_outlet - 1] = new_freq

		# self.relay_controller.flash_outlet(self.selected_outlet, new_freq)
		self.redis.publish(self.flash_outlet_pub, json.dumps({"outlet_number": self.selected_outlet, "freq": new_freq}))  # publishes to flash relay outlet

		print("{}".format(self.relay_freqs))

	def pb_press_callback(self, channel):
		"""
		Handles push button event.
		Checks current mode, and either toggles or flashes
		outlet based on mode.
		"""
		sleep(0.01)  # 10ms
		if not GPIO.input(channel):
			return
		print("PB press registered from channel: {}".format(channel))
		pin_index = None
		try:
			pin_index = self.pb_pins.index(channel)
		except ValueError as e:
			print("Pin {} channel not in list: {}".format(channel, self.pb_pins))
			return False
		if self.control_mode == 'toggle':
			print("Toggling outlet.")
			# self.relay_controller.toggle_outlet(pin_index + 1)  # turns led on/off on control board
			
			# publish to relay controller:
			self.redis.publish(self.toggle_outlet_pub, json.dumps({"outlet_number": pin_index + 1}))

		elif self.control_mode == 'flash':
			print("Flashing outlet.")

			freq = self.default_freq
			if self.relay_freqs[pin_index]:
				freq = relay_freq

			self.relay_freqs[pin_index] = freq

			self.redis.publish(self.flash_outlet_pub, json.dumps({"outlet_number": pin_index + 1, "freq": freq}))

			self.selected_outlet = pin_index + 1  # stores selected outlet for changing freq w/ dail
			# TODO: publish selected_outlet for other nodes
			self.redis.publish(self.selected_outlet_pub, json.dumps(self.selected_outlet))

		return True

	def rot_clK_callback(self, channel):
		"""
		Handles rotary encoder callback for turning dial (CLK).
		"""
		return  # NOTE: (1/1/21) disabling flashing mode.
		# sleep(0.01)
		# clk_state = GPIO.input(self.rot_clk_pin)
		# dt_state = GPIO.input(self.rot_dt_pin)
		# if clk_state != self.last_state:
		# 	if dt_state != clk_state:
		# 		self.counter += 1
		# 		if self.counter % self.count_increment == 0:
		# 			self.handle_freq_change('increment')
		# 	else:
		# 		self.counter -= 1
		# 		if self.counter % self.count_increment == 0:
		# 			self.handle_freq_change('decrement')
		# 	print("Counter: {}".format(self.counter))
		# self.last_state = clk_state

	def rot_sw_callback(self, channel):
		"""
		Handles rotary encoder callback for pressing dial (SW).
		Switches between modes (toggle, flashing, etc.).
		"""
		return  # NOTE: (1/1/21) disabling flashing mode.
		# sleep(0.01)
		# if GPIO.input(channel):
		# 	# NOTE: 0 means rotary button was pressed
		# 	return
		# print("rot_sw_callback hit: {}".format(channel))
		# # Switches to toggle mode:
		# if self.control_mode == 'flash':
		# 	print("Switching to toggle mode.")
		# 	if not self.mode_pwm:
		# 		self.mode_pwm = GPIO.PWM(self.mode_led_pin, 2.0)
		# 		self.mode_pwm.start(100)
		# 	else:
		# 		self.mode_pwm.ChangeDutyCycle(100)
		# 	self.control_mode = 'toggle'
		# 	self.selected_outlet = None
		# # Switches to flash mode:
		# elif self.control_mode == 'toggle':
		# 	print("Switching to flash mode.")
		# 	if not self.mode_pwm:
		# 		self.mode_pwm = GPIO.PWM(self.mode_led_pin, 2.0)  # flashes at 2Hz
		# 		self.mode_pwm.start(50)  # 50% DC
		# 	else:
		# 		self.mode_pwm.ChangeDutyCycle(50)
		# 	self.control_mode = 'flash'

		# # publishes control mode update:
		# self.redis.publish(self.control_mode_pub, json.dumps(self.control_mode))

	def cleanup(self):
		GPIO.cleanup()

	def main(self):
		self.setup_gpio()
		self.setup_leds_gpio()



if __name__ == '__main__':

	control_module = ControlModule()

	try:

		print("Initiating control_module.")
		print("Running control_module main function.")

		control_module.main()

		while True:
			sleep(0.01)

	except Exception as e:
		print("Exception occurred: {}".format(e))
		print("Shutting down control_module controls.")
		control_module.cleanup()
