
class MainControlPanelPins:
	def __init__(self):
		self.power_supply_output = 22  # turns main power supply on/off (green wire on PS)
		self.green_power_led = 1  # pin number for green power led (3.3V pin, not GPIO)
		self.red_flashing_led = 24  # pin number for red flashing led (main PS is on)
		self.push_button_led = 26  # led inside push button that starts main power supply
		self.start_power_button = 32  # push button that initiates turning on main power supply

class RelayControlsPins:
	def __init__(self):
		self.relay_power_pin = 4  # 3.3v power
		self.relay_ground_pin = 9
		self.relay_pins = [3, 5, 7, 11, 13, 15, 19, 21]  # maps pins to outlets 1 - 8 (indices 0 - 7, respectively)

class ControlModulePins:
	def __init__(self):

		self.pb_pins = [29, 31, 33, 35, 37, 36, 38, 40]  # pb 1 - 8 pins (inputs)
		self.led_pins = [3, 5, 7, 11, 13, 15, 19, 21]  # pb leds 1 - 8 (outputs)

		# Defines rotary encoder pins:
		self.rot_clk_pin = 12
		self.rot_dt_pin = 16
		self.rot_sw_pin = 18
		self.mode_led_pin = 23  # led to indicate control mode (e.g., toggle, flash)
