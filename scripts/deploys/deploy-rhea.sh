#!/bin/bash

############################################
# Deploys relay API for Rhea.
############################################
# 1. Relay controls
############################################

export REDIS_HOST=192.168.0.200

cd /home/gladis/GitHub/spaceship_controls/
source env/bin/activate

# 1. Runs relay (with relay node) and video API:
env/bin/python app.py &

# 2. Starts playing video with default settings:
curl http://localhost:5000/video/play/folder
