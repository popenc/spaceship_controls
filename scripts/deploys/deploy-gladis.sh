#!/bin/bash

export REDIS_HOST=192.168.0.200

cd /home/gladis/GitHub/spaceship_controls/
source env/bin/activate

# Runs main control panel:
env/bin/python main_control_panel/control_panel.py &

# Runs control module (8 buttons for relay):
env/bin/python control_module/control_module_node.py &




