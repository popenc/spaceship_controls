#!/bin/bash

# Sets up raspberry pi 3 B+.
# Includes:
#   * Python
#   * spaceship_controls
#   * 

# TODO: Install Python 3.x, rpi default python3 is probably too low.
# TODO: Set any files for startup scripts.

curl -O https://bootstrap.pypa.io/get-pip.py
pip apt-get install -y python-distutils python-dev
sudo python3 get-pip.py
python3 -m pip install virtualenv

mkdir GitHub
cd GitHub/
git clone https://bitbucket.org/popenc/spaceship_controls
cd spaceship_controls/

# virtualenv env
# source env/bin/activate
# sudo pip install -r requirements.txt

# Installs Docker and Docker Compose
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
rm get-docker.sh
sudo usermod -aG docker $USER

sudo apt-get update
sudo apt-get install -y libffi-dev libssl-dev
# sudo apt-get install -y python3 python3-pip
sudo pip3 -v install docker-compose