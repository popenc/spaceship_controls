from flask import Blueprint, jsonify
import os
import sys
import redis

# Local imports:
# from .relay_node import RelayNode
from .relay_controller import RelayController
from .relay_controller_plate import RelayControllerPlate, RelayNode, check_relay_plate

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(1, os.path.join(PROJECT_ROOT, ".."))

# Loads local environment:
from environment import load_env
load_env()

relay_blueprint = Blueprint("relay", __name__)

rc = None
if check_relay_plate():
	rc = RelayControllerPlate()
else:
	rc = RelayController()  # relay controller instance

rn = RelayNode()  # initializes redis node for relay controller
rn.relay_controller = rc  # sets redis node relay_controller instance to api's rc instance
rn.start_node()  # defines redis, channels, and subs/pubs
# rc.redis = rn.redis
# rc.relay_states_pub = rn.relay_states_pub
# rc.relay_freqs_pub = rn.relay_freqs_pub



@relay_blueprint.route("/relay", methods=["GET"])
def relay_api():
    return {
    	"meta": {
	    	"outlets": [1, 2, 3, 4, 5, 6, 7],
	    	"num_outlets": 7
	    },
    	"endpoints": [
    		"/relay",
			"/relay/off/<string:outlet_number>",
			"/relay/toggle/<string:outlet_number>",
			"/relay/flash/<string:outlet_number>/<float:freq>",
			"/relay/preset/<string:preset>"
		]
    }

@relay_blueprint.route("/relay/off/<string:outlet_number>", methods=["GET"])
def turn_off_outlet(outlet_number):
	# TODO: Error, handling, input validation, etc.
	if outlet_number == "all":
		rc.turn_off_outlets()
		return jsonify({'status': "success"})
	outlets = rc.parse_outlet_number(outlet_number)
	rc.turn_off_outlet(outlets)
	return jsonify({'status': "success"})

@relay_blueprint.route("/relay/toggle/<string:outlet_number>", methods=["GET"])
def toggle_outlet(outlet_number):
	# TODO: Error, handling, input validation, etc.
	outlets = rc.parse_outlet_number(outlet_number)
	rc.toggle_outlet(outlets)
	return jsonify({'status': "success"})

@relay_blueprint.route("/relay/flash/<string:outlet_number>/<float:freq>", methods=["GET"])
def flash_outlet(outlet_number, freq):
	# TODO: Error, handling, input validation, etc.
	outlets = rc.parse_outlet_number(outlet_number)
	print("Outlets: {}".format(outlets))
	rc.flash_outlet(outlets, freq)
	return jsonify({'status': "success"})

@relay_blueprint.route("/relay/preset/<string:preset>", methods=["GET"])
def set_preset(outlet_number, freq):
	"""
	Sets relay (lighting/power) to a preset configuration.
	"""
	# TODO: Handling list (or string with commas) of outlets
	# rc.flash_outlet(outlet_number, freq)
	pass

# @relay_blueprint.route("/relay/valid/freq/<int:freq>", methods=["GET"])
# def validate_freq(freq):
# 	return rc.is_valid_frequency(freq)
