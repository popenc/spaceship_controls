# Spaceship lighting
lights = [
	{
		"id": 1,
		"name": "red1",
		"description": "Red light behind oscilloscope, the device with a red and blue light attached together."
	},
	{
		"id": 2,
		"name": "blue1",
		"description": "Blue light behind oscilloscope, the device with a red and blue light attached together."
	},
	{
		"id": 3,
		"name": "red2",
		"description": "Red light in lower wall stage right."
	},
	{
		"id": 4,
		"name": "red3",
		"description": "Red light in lower wall stage left."
	},
	{
		"id": 5,
		"name": "blue2",
		"description": "Blue left in back corner stage left (TODO)."
	},
	{
		"id": 6,
		"name": "stage1",
		"description": "Flashing ball lights that line stage."
	},
	{
		"id": 7,
		"name": "carl",
		"description": "The light that lights up Carl Sagan."
	},
	{
		"id": 8,
		"name": "angel",
		"description": "Multi-color light inside bass drum."
	}
]