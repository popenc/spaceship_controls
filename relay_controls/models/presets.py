"""
Preset configurations for relay.
"""

# Mapping which lights are connected to which outlets/relays:
relay_map = [
	{
		"outlet": 1,
		"devices": ["red1"],
		"description": ""
	},
	{
		"outlet": 2,
		"devices": ["blue1", "blue2"],
		"description": ""
	},
	{
		"outlet": 3,
		"devices": ["red2", "red3"],
		"description": ""
	},
	{
		"outlet": 4,
		"devices": ["stage1"],
		"description": ""
	},
	{
		"outlet": 5,
		"devices": ["carl"],
		"description": ""
	}
	{
		"outlet": 6,
		"devices": ["angel"],
		"description": ""
	}
	{
		"outlet": 7,
		"devices": ["monitor1", "monitor2"],
		"description": ""
	}
]

# Defined presets:
presets = [
	{
		"id": 1,
		"name": "red1",
		"description": "All red lights.",
		"lights": ["red1", "red2", "red3"],  # NOTE: Handle devices separately?
		"outlets_on": [1, 3],
		"outlets_off": [2]
	},
	{
		"id": None,
		"name": "blue",
		"description": "All blue lights.",
		"lights": ["blue1", "blue2"],
		"outlets_on": [2],
		"outlets_off": [1, 3]
	},
	{
		"id": None,
		"name": "dark1",
		"description": "One red light and just carl.",
		"lights": ["red1"],
		"outlets_on": [1],
		"outlets_off": [2, 3]
	},
	{
		"id": None,
		"name": "all-on",
		"description": "Every light on.",
		"lights": ["red1", "red2", "red3", "blue1", "blue2"],
		"outlets_on": [1, 2, 3, 4, 5, 6],
		"outlets_off": []
	},
	{
		"id": None,
		"name": "all-off",
		"description": "Every light off.",
		"lights": [],
		"outlets_on": [],
		"outlets_off": [1, 2, 3, 4, 5, 6]
	}
]