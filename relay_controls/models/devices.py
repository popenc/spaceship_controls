# Spaceship relay devices:
devices = [
	{
		"id": 1,
		"name": "monitor1",
		"description": "Main monitor with control panel."
	},
	{
		"id": 2,
		"name": "monitor2",
		"description": "Secondary monitor controlled by pi zero."
	},
	{
		"id": 3,
		"name": "scope",
		"description": "The oscilloscope."
	},
	{
		"id": 4,
		"name": "tree",
		"description": "The blue christmas tree."
	},
	{
		"id": 5,
		"name": "skeleton",
		"description": "The skeleton."
	}
]