import RPi.GPIO as GPIO
import piplates.RELAYplate as RELAY
from time import sleep
import redis
import json
import threading
import os
import sys
import multiprocessing

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(1, os.path.join(PROJECT_ROOT, ".."))

# Loads local environment:
from environment import load_env
load_env()


def check_relay_plate():
	try:
		print("Relay ID: {}".format(RELAY.getID(0)))
		return True
	except AssertionError:
		print("No RELAYplate found. Defaulting to GPIO library.")
		return False


class RelayControllerPlate:
	"""
	Controls the relays for the 8-outlets.
	
	(09-19-20) Using RELAYPlate now and separate Pi
	(https://pi-plates.com/product/relayplate/).
	"""

	def __init__(self):

		self.relay_state_options = ["on", "off", "flashing"]
		self.relay_states = ['off', 'off', 'off', 'off', 'off', 'off', 'off']  # states: on/off/flashing
		self.relay_freqs = [None, None, None, None, None, None, None]  # freq value per outlet

		self.relay_threads = [None, None, None, None, None, None, None]  # keeps track of outlet flash threads

		# Frequency range settings:
		self.count_increment = 1  # for every n counts, go up/down freq by freq_step
		self.min_freq = 0.1
		self.max_freq = 5.1

		# Redis configuration:
		self.redis = None  # redis instance used to publish (from relay_node.py, where sub is located)
		self.relay_freqs_pub = None
		self.relay_states_pub = None

		self.initialized = False  # flag used so other nodes don't setup relay if it's already set up.

	def parse_outlet_number(self, outlet_number):
		"""
		outlet_number examples: "1"; "1,2,5"; "all"; "1-4"
		"""
		outlet_numbers = []
		if isinstance(outlet_number, int):
			outlet_numbers.append(outlet_number)
		elif isinstance(outlet_number, float):
			outlet_numbers.append(int(outlet_number))
		elif len(outlet_number.split(",")) > 1:
			# outlet numbers separated by commas (e.g., "1,2,4")
			outlet_numbers += [int(i) for i in outlet_number.split(",")]
		elif len(outlet_number.split("-")) > 1:
			# outlet numbers provided as range (e.g., "1-3")
			outlet_numbers += [int(i) for i in outlet_number.split("-")]
		elif outlet_number == "all":
			# full range of outlets (currently 1 - 7)
			outlet_numbers += [i for i in range(1, len(self.relay_states) + 1)]
		else:
			# assuming single outlet number (e.g., "1")
			outlet_numbers.append(int(outlet_number))
		return outlet_numbers

	def convert_list_to_decbin(self):
		"""
		Converts relay state list to binary decimal when updating relay states.
		Ex: ['on','on','on','on','on','on','on'] = 1111111 = 127.
		"""
		binary_string = ""
		# for outlet in outlet_numbers:
		# 	if outletDS
		for state in reversed(self.relay_states):
			# counting starts at last outlet and goes backwards.
			if state == "on":
				binary_string += "1"
			elif state == "off":
				binary_string += "0"
		print("Binary string: {}".format(binary_string))
		return int(binary_string, 2)

	def is_valid_outlet_number(self, outlet_numbers):
		"""
		Error checking for outlet number.
		"""
		print("VALID OUTLET NUMBER CHECK: {}".format(outlet_numbers))
		try:
			for outlet_number in outlet_numbers:
				outlet_number = float(outlet_number)
				if outlet_number < 1 or outlet_number > len(self.relay_states):
					return False
		except Exception as e:
			print("Couldn't convert outlet number to float: {}".format(e))
			return False
		return True

	def is_valid_freq(self, freq):
		"""
		Error checking for frequency.
		"""
		print("Checking if valid freq. {}".format(freq))
		try:
			freq = float(freq)
			if freq < self.min_freq or freq > self.max_freq:
				return False
		except Exception as e:
			print("Couldn't convert freq to float: {}".format(e))
		return True

	def turn_off_outlets(self):
		"""
		Turns off all outlets.
		"""
		for i in range(0, len(self.relay_states)):
			self.relay_states[i] = 'off'
			self.stop_thread(i + 1)

		RELAY.relayALL(0, 0)  # turns off all outlets

		print("Updated threads: {}".format(self.relay_threads))
		print("Publish updated relay state: {}".format(self.relay_states))

		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))


	def turn_off_outlet(self, outlet_numbers):
		"""
		Turns off a single outlet.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}

		for outlet_number in outlet_numbers:
			self.stop_thread(outlet_number)
			self.relay_states[outlet_number - 1] = 'off'

		relay_val = self.convert_list_to_decbin()
		print("Relay value: {}".format(relay_val))
		RELAY.relayALL(0, relay_val)

		# publish updated relay state
		print("Publish updated relay state: {}".format(self.relay_states))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))


	def toggle_outlet(self, outlet_numbers):
		"""
		Turns on outlet on/off.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}

		for outlet_number in outlet_numbers:
		
			relay_state = self.relay_states[outlet_number - 1]
			relay_thread = self.relay_threads[outlet_number - 1]
			
			print("Toggle outlet, state: {}, thread: {}".format(relay_state, relay_thread))

			if relay_thread:
				# Stops flashing thread:
				self.relay_threads[outlet_number - 1]._run = False
				self.relay_threads[outlet_number - 1] = None

			if relay_state == 'on' or relay_state == 'flashing':
				# Turns off outlet if it was on:
				# RELAY.relayOFF(0, outlet_number)
				self.relay_states[outlet_number - 1] = 'off'
			elif relay_state == 'off':
				# Turns on outlet if it was off:
				# RELAY.relayON(0, outlet_number)
				self.relay_states[outlet_number - 1] = 'on'

		relay_val = self.convert_list_to_decbin()
		print("Relay value: {}".format(relay_val))
		RELAY.relayALL(0, relay_val)

		# publish updated relay state
		print("Publish updated relay state: {}".format(self.relay_states))
		# self.redis.publish(self.relay_state_pub, json.dumps(self.relay_data))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))

	def flash_outlet(self, outlet_numbers, freq):
		"""
		Sets relay/outlet's PWM to flash at a given frequency,
		defaults duty cycle to 50%.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}
		if not self.is_valid_freq(freq):
			return {'status': False, 'error': "Invalid freq value."}

		for outlet_number in outlet_numbers:

			relay_state = self.relay_states[outlet_number - 1]

			self.stop_thread(outlet_number)
			
			# Starts thread for flashing:
			self.relay_threads[outlet_number - 1] = threading.Thread(target=self.flash_thread, args=(outlet_number, freq))
			self.relay_threads[outlet_number - 1].start()

			self.relay_freqs[outlet_number - 1] = freq  # keeps track of each outlet's frequency
			self.relay_states[outlet_number - 1] = 'flashing'  # sets outlet state

		# publish updated relay state
		print("Publish updated relay state and freq: {}, {}".format(self.relay_states, self.relay_freqs))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))
		return True

	def stop_thread(self, outlet_number):
		# Stops existing thread:
		if self.relay_threads[outlet_number - 1]:
			self.relay_threads[outlet_number - 1]._run = False
			self.relay_threads[outlet_number - 1] = None

	def flash_thread(self, outlet_number, freq):
		"""
		Loop for flashing outlet in a thread.

		TODO: Flash thread that turns list on/off with RELAY.all (or whatever it is)
		"""
		try:
			freq = float(freq)
			t = threading.currentThread()
			while getattr(t, "_run", True):
				sleep(1.0/freq)
				print("Flashing '{}' on".format(outlet_number))
				RELAY.relayON(0, outlet_number)
				self.relay_states[outlet_number - 1] = 'on'
				self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))
				sleep(1.0/freq)
				print("Flashing '{}' off".format(outlet_number))
				RELAY.relayOFF(0, outlet_number)
				self.relay_states[outlet_number - 1] = 'off'
				self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))
			print("Thread for '{}' stopped.".format(outlet_number))
		except Exception as e:
			print("Exception in flash_thread: {}".format(e))
			print("TODO: Stop thread(s): '{}'".format(outlet_number))
			self.relay_threads[outlet_number - 1]._run = False
			self.relay_threads[outlet_number - 1] = None

	def cleanup(self):
		GPIO.cleanup()



class RelayNode:

	def __init__(self):

		self.relay_controller = RelayControllerPlate()

		self.redis = redis.Redis(host=os.environ.get('REDIS_HOST', 'localhost'), port=os.environ.get('REDIS_PORT', 6379), db=0)
		self.pubsub = self.redis.pubsub()
		self.sub_thread = None

		# Subscribers:
		# self.sub = "relay-state-sub"
		self.turn_off_outlet_sub = "relay-off-sub"
		self.turn_on_outlet_sub = "relay-on-sub"
		self.toggle_outlet_sub = "relay-toggle-sub"
		self.flash_outlet_sub = "relay-flash-sub"

		# Publishers:
		self.relay_states_pub = "relay-states-pub"
		self.relay_freqs_pub = "relay-freqs-pub"


	def init_sub(self):
		self.pubsub.psubscribe(**{self.turn_off_outlet_sub: self.turn_off_outlet_callback})
		self.pubsub.psubscribe(**{self.toggle_outlet_sub: self.toggle_outlet_callback})
		self.pubsub.psubscribe(**{self.flash_outlet_sub: self.flash_outlet_callback})
		self.pubsub.psubscribe(**{self.relay_states_pub: self.relay_state_callback})
		self.sub_thread = self.pubsub.run_in_thread(sleep_time=0.001)  # runs sub in separate thread (thread.stop() to kill)
	

	def turn_off_outlet_callback(self, message):
		print("turn_off_outlet_callback message: {}".format(message))
		data_obj = json.loads(message['data'])
		outlet_number = data_obj['outlet_number']  # int or "all"
		if outlet_number == "all":
			self.turn_off_outlets()
			return
		outlets = self.relay_controller.parse_outlet_number(outlet_number)
		self.relay_controller.toggle_outlet(outlets)

	def toggle_outlet_callback(self, message):
		print("toggle_outlet_callback message: {}".format(message))
		data_obj = json.loads(message['data'])
		outlet_number = data_obj['outlet_number']
		outlets = self.relay_controller.parse_outlet_number(outlet_number)
		self.relay_controller.toggle_outlet(outlets)

	def flash_outlet_callback(self, message):
		print("flash_outlet_callback message: {}".format(message))
		data_obj = json.loads(message['data'])
		outlet_number = data_obj['outlet_number']
		freq = data_obj['freq']
		outlets = self.relay_controller.parse_outlet_number(outlet_number)
		self.relay_controller.flash_outlet(outlets, freq)

	def relay_state_callback(self, message):
		print("relay_state_callback updated relay state: {}".format(message))
		data_obj = json.loads(message['data'])
		self.relay_controller.relay_states = data_obj

	def stop_node(self):
		if self.sub_thread:
			self.sub_thread.stop()  # stops sub thread
		self.relay_controller.cleanup()  # cleans up GPIO

	def start_node(self):
		self.relay_controller.redis = self.redis
		self.relay_controller.relay_states_pub = self.relay_states_pub
		self.relay_controller.relay_freqs_pub = self.relay_freqs_pub
		self.init_sub()  # starts sub thread





if __name__ == '__main__':

	relay_node = RelayNode()

	print("Initiating relay_node.")
	print("Running relay_node start_node function.")

	relay_node.start_node()

	while True:
		sleep(0.01)
