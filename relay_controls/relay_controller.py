import RPi.GPIO as GPIO
# import piplates.RELAYplate as RELAY
from time import sleep
import redis
import json


class RelayController:
	"""
	Controls the relays for the 8-outlets.

	This version using the standard RPi.GPIO library
	(i.e., not RELAYplate) and is now used to control the
	LEDs of the control module?
	NOTE: Actually, may make the most sense to actually
	have everything in control_module.
	"""

	def __init__(self):
		# Defines pins:
		self.relay_default_freq = 1.0  # 1.0Hz PWM
		self.relay_power_pin = 4
		self.relay_ground_pin = 9
		self.relay_pins = [3, 5, 7, 11, 13, 15, 19, 21]  # maps pins to outlets 1 - 8 (indices 0 - 7, respectively)

		self.relay_state_options = ["on", "off", "flashing"]
		self.relay_states = ['off', 'off', 'off', 'off', 'off', 'off', 'off', 'off']  # states: on/off/flashing
		self.relay_freqs = [None, None, None, None, None, None, None, None]  # freq value per outlet
		
		self.relay_pwms = [None, None, None, None, None, None, None, None]  # pwm gpio instance for each outlet

		# Frequency range settings:
		self.count_increment = 1  # for every n counts, go up/down freq by freq_step
		self.min_freq = 0.1
		self.max_freq = 5.1

		# Redis configuration:
		self.redis = None  # redis instance used to publish (from relay_node.py, where sub is located)
		# self.relay_state_pub = None  # publisher topic name
		self.relay_freqs_pub = None
		self.relay_states_pub = None

		self.initialized = False  # flag used so other nodes don't setup relay if it's already set up.

	def setup_gpio(self):
		"""
		Sets up RPi GPIO pins.
		"""
		GPIO.setmode(GPIO.BOARD)
		relay_index = 0
		for relay_pin in self.relay_pins:
			print("Setting up relay pin {} as output.".format(relay_pin))
			GPIO.setup(relay_pin, GPIO.OUT)  # sets as OUTPUT prior to PWM instance
			self.relay_freqs[relay_index] = self.relay_default_freq  # initialize freqs with default value
			relay_index += 1

		self.redis.publish(self.relay_freqs_pub, json.dumps(self.relay_freqs))
		self.turn_off_outlets()

	def parse_outlet_number(self, outlet_number):
		"""
		outlet_number examples: "1"; "1,2,5"; "all"; "1-4"
		"""
		outlet_numbers = []
		if len(outlet_number.split(",")) > 1:
			# outlet numbers separated by commas (e.g., "1,2,4")
			outlet_numbers += [int(i) for i in outlet_number.split(",")]
		elif len(outlet_number.split("-")) > 1:
			# outlet numbers provided as range (e.g., "1-3")
			outlet_numbers += [int(i) for i in outlet_number.split("-")]
		elif outlet_number == "all":
			# full range of outlets (currently 1 - 7)
			outlet_number += [i for i in range(1, len(rc.relay_states))]
		else:
			# assuming single outlet number (e.g., "1")
			outlet_numbers.append(int(outlet_number))
		return outlet_numbers

	# def is_valid_outlet_number(self, outlet_number):
	# 	"""
	# 	Error checking for outlet number.
	# 	"""
	# 	try:
	# 		outlet_number = float(outlet_number)
	# 	except Exception as e:
	# 		print("Couldn't convert outlet number to float: {}".format(e))
	# 		return False
	# 	if outlet_number < 1 or outlet_number > 8:
	# 		return False
	# 	return True
	def is_valid_outlet_number(self, outlet_numbers):
		"""
		Error checking for outlet number.
		"""
		try:
			for outlet_number in outlet_numbers:
				outlet_number = float(outlet_number)
				if outlet_number < 1 or outlet_number > len(self.relay_states):
					return False
		except Exception as e:
			print("Couldn't convert outlet number to float: {}".format(e))
			return False
		return True

	def is_valid_freq(self, freq):
		"""
		Error checking for frequency.
		"""
		try:
			freq = float(freq)
		except Exception as e:
			print("Couldn't convert freq to float: {}".format(e))
		if freq < self.min_freq or freq > self.max_freq:
			return False
		return True

	def turn_off_outlets(self):
		"""
		Turns off all outlets.
		"""
		relay_index = 0
		#for relay_pwm in self.relay_pwms:
		for relay_pin in self.relay_pins:
			if self.relay_pwms[relay_index]:
				self.relay_pwms[relay_index].stop()  # stops any PWMs
				# self.relay_freqs[relay_index] = None
			GPIO.output(relay_pin, GPIO.HIGH)  # NOTE: Outlets wired normally-closed
			self.relay_states[relay_index] = 'off'
			relay_index += 1

		print("Publish updated relay state: {}".format(self.relay_states))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))

	def turn_off_outlet(self, outlet_numbers):
		"""
		Turns off a single outlet.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}

		for outlet_number in outlet_numbers:
			if self.relay_pwms[outlet_number - 1]:
				self.relay_pwms[outlet_number - 1].stop()
				# self.relay_freqs[outlet_number - 1] = None
			GPIO.output(self.relay_pins[outlet_number - 1], GPIO.HIGH)
			self.relay_states[outlet_number - 1] = 'off'

		# publish updated relay state
		print("Publish updated relay state: {}".format(self.relay_states))
		# self.redis.publish(self.relay_state_pub, json.dumps(self.relay_data))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))

	def toggle_outlet(self, outlet_numbers):
		"""
		Turns on outlet on/off.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}

		for outlet_number in outlet_numbers:
			relay_state = self.relay_states[outlet_number - 1]
			relay_pwm = self.relay_pwms[outlet_number - 1]
			if relay_pwm and relay_state == 'flashing':
				# Stops PWM:
				self.relay_pwms[outlet_number - 1].stop()
				self.relay_pwms[outlet_number - 1] = None
			if relay_state == 'on' or relay_state == 'flashing':
				# Turns off outlet if it was on:
				GPIO.output(self.relay_pins[outlet_number - 1], GPIO.HIGH)
				self.relay_states[outlet_number - 1] = 'off'
			elif relay_state == 'off':
				# Turns on outlet if it was off:
				GPIO.output(self.relay_pins[outlet_number - 1], GPIO.LOW)
				self.relay_states[outlet_number - 1] = 'on'

		# publish updated relay state
		print("Publish updated relay state: {}".format(self.relay_states))
		# self.redis.publish(self.relay_state_pub, json.dumps(self.relay_data))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))

	def flash_outlet(self, outlet_numbers, freq):
		"""
		Sets relay/outlet's PWM to flash at a given frequency,
		defaults duty cycle to 50%.
		"""
		if not self.is_valid_outlet_number(outlet_numbers):
			return {'status': False, 'error': "Invalid outlet number."}
		if not self.is_valid_freq(freq):
			return {'status': False, 'error': "Invalid freq value."}

		for outlet_number in outlet_numbers:
			relay_pwm = self.relay_pwms[outlet_number - 1]
			relay_state = self.relay_states[outlet_number - 1]
			relay_pin = self.relay_pins[outlet_number - 1]
			if not relay_pwm:
				self.relay_pwms[outlet_number - 1] = GPIO.PWM(relay_pin, self.relay_default_freq)  # default to 0.5Hz flash
				self.relay_pwms[outlet_number - 1].start(50)  # 50% DC
			self.relay_pwms[outlet_number - 1].ChangeFrequency(freq)  # NOTE: Might be faster to use threads and toggle pins as OUTPUT instead of PWM (if this method doesn't work)
			self.relay_freqs[outlet_number - 1] = freq  # keeps track of each outlet's frequency
			self.relay_states[outlet_number - 1] = 'flashing'  # sets outlet state

		# publish updated relay state
		print("Publish updated relay state and freq: {}, {}".format(self.relay_states, self.relay_freqs))
		# self.redis.publish(self.relay_state_pub, json.dumps(self.relay_data))
		self.redis.publish(self.relay_states_pub, json.dumps(self.relay_states))
		self.redis.publish(self.relay_freqs_pub, json.dumps(self.relay_freqs))

		return True

	def set_preset(self, preset):
		"""
		Sets toggles (and flashes) lights based on preset configs.
		"""
		# Get object that matches preset name:
		# Turn on and off outlets:
		# Set any that should flash:
		pass

	def cleanup(self):
		GPIO.cleanup()

	def main(self):
		self.setup_gpio()



if __name__ == '__main__':

	relay_controller = RelayController()

	try:

		print("Initiating relay_controller.")
		print("Running relay_controller main function.")

		relay_controller.main()

		while True:
			sleep(0.01)

	except Exception as e:
		print("Exception occurred: {}".format(e))
		print("Shutting down relay_controller controls.")
		relay_controller.cleanup()