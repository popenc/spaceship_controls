import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

gpio_list = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]

for gpio in gpio_list:

	GPIO.setup(gpio, GPIO.IN)

	print("Current gpio value: {}".format(GPIO.input(gpio)))

	GPIO.setup(gpio, GPIO.OUT)

	print("Turning LOW")

	time.sleep(0.5)

	GPIO.output(gpio, GPIO.LOW)

	print("Turning HIGH")

	time.sleep(0.5)

	GPIO.output(gpio, GPIO.HIGH)

	print("Turning LOW")

	time.sleep(0.5)

	GPIO.output(gpio, GPIO.LOW)

print("Cleaning up gpio")

time.sleep(0.5)

GPIO.cleanup()