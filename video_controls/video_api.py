from flask import Blueprint, jsonify
import os
import subprocess

# Local imports:
from . import video_controller


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

video_blueprint = Blueprint("video", __name__)


player_obj = None


@video_blueprint.route("/video", methods=["GET"])
def video_api():
    # return "Video API (TODO: list endpoints)."
    return {
    	"meta": {
	    	"video_folder": "spaceship_videos",
	    	"video_folders": ["originals", "timelapse", "shorts"],
	    },
    	"endpoints": [
    		"/video/play/<string:video>",
			"/video/play/folder/<int:delay>/<string:folder>",
			"/video/stop",
			"/video/kill",
			"/video/pause",
			"/video/volume/up",
			"/video/volume/down",
			"/video/speed/up",
			"/video/speed/down",
			"/video/prev",
			"/video/next",
		]
    }

@video_blueprint.route("/video/play/<string:video>", methods=["GET"])
def play_video(video=None):
	"""
	Plays videos in a given folder.
	Returns omxplayer PID.
	"""
	global player_obj
	if player_obj:
		return "Player is already active (pid: {})".format(player_obj.pid)
	player_obj = video_controller.play_video(video)
	return str(player_obj.pid)

@video_blueprint.route("/video/play/folder/<int:delay>/<string:folder>", methods=["GET"])
def play_folder(delay=180, folder="originals"):
	"""
	Plays videos in a given folder.
	Returns omxplayer PID.
	"""
	global player_obj
	if player_obj:
		return "Player is already active (pid: {})".format(player_obj.pid)
	video_controller.play_folder(delay, folder)
	return jsonify({"status": "omxplayer finished playing."})

@video_blueprint.route("/video/stop", methods=["GET"])
def stop_video():
	"""
	Stops video.
	"""
	global player_obj
	player_obj.stdin.write(b"q")
	return jsonify({"status": "omxplayer process stopped."})


@video_blueprint.route("/video/kill", methods=["GET"])
def kill_omxplayer():
	"""
	Kills all omxplayer processes.
	"""
	subprocess.run(["killall", "omxplayer.bin"])
	return jsonify({"status": "omxplayer processes killed."})

@video_blueprint.route("/video/pause", methods=["GET"])
def pause_video():
	"""
	Pauses video.
	"""
	global player_obj
	player_obj.stdin.write(b"p")
	return jsonify({"status": "omxplayer video paused."})

@video_blueprint.route("/video/volume/up", methods=["GET"])
def volume_up():
	"""
	Turns volume up.
	"""
	global player_obj
	player_obj.stdin.write(b"+")
	return jsonify({"status": "omxplayer volume up."})

@video_blueprint.route("/video/volume/down", methods=["GET"])
def volume_down():
	"""
	Turns volume down.
	"""
	global player_obj
	player_obj.stdin.write(b"-")
	return jsonify({"status": "omxplayer volume down."})

@video_blueprint.route("/video/mute", methods=["GET"])
def mute():
	"""
	Mutes audio.
	"""
	subprocess.run(["amixer", "sset", "PCM,0 100%"])
	# Or maybe: amixer -c 0 Master playback 0% mute
	# Or maybe: amixer -c 0 Master playback 100% unmute

@video_blueprint.route("/video/unmute", methods=["GET"])
def unmute():
	"""
	Unmutes audio.
	"""
	subprocess.run(["amixer", "sset", "PCM,0 0%"])
	# Or maybe: amixer -c 0 Master playback 0% mute
	# Or maybe: amixer -c 0 Master playback 100% unmute
	

@video_blueprint.route("/video/speed/up", methods=["GET"])
def speed_up():
	"""
	Turns speed up.
	"""
	global player_obj
	player_obj.stdin.write(b"2")
	return jsonify({"status": "omxplayer speed up."})

@video_blueprint.route("/video/speed/down", methods=["GET"])
def speed_down():
	"""
	Turns speed down.
	"""
	global player_obj
	player_obj.stdin.write(b"1")
	return jsonify({"status": "omxplayer speed down."})

@video_blueprint.route("/video/prev", methods=["GET"])
def prev_audio():
	"""
	Previous audio stream.
	"""
	global player_obj
	player_obj.stdin.write(b"j")
	return jsonify({"status": "omxplayer previous audio."})

@video_blueprint.route("/video/next", methods=["GET"])
def next_audio():
	"""
	Next audio stream.
	"""
	global player_obj
	player_obj.stdin.write(b"k")
	return jsonify({"status": "omxplayer next audio."})
