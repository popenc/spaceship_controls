import subprocess
import os
# import keyboard
import time
import requests
import sys

# PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

# NOTE: Below assumes "spaceship_videos" adjacent to "spaceship_controls" repo
PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))


print("PROJECT_ROOT: {}".format(PROJECT_ROOT))

def play_video(filename=None, folder='originals'):
	video_dir = os.path.join(PROJECT_ROOT, 'spaceship_videos', folder)
	print("VIDEO DIR: {}".format(video_dir))
	while True:
		if not filename:
			omxplayer = subprocess.Popen(['/usr/bin/omxplayer', '--loop', '--vol', '-6000', os.path.join(video_dir, 'alcohol.mp4')], stdin=subprocess.PIPE, stdout=None, stderr=None, bufsize=0)
		else:
			omxplayer = subprocess.Popen(['/usr/bin/omxplayer', '--loop', filename], stdin=subprocess.PIPE, stdout=None, stderr=None, bufsize=0)
	return omxplayer

def play_folder(delay=30, folder='originals'):
	video_dir = os.path.join(PROJECT_ROOT, 'spaceship_videos', folder)
	print("VIDEO DIR: {}".format(video_dir))
	while True:
		for filename in os.listdir(video_dir):
			if ".py" in filename:
				print("Skipping {}".format(filename))
				continue
			video_file = os.path.join(video_dir, filename)
			# video_length = get_video_length(video_file)
			print("Playing video: {}".format(video_file))
			# print("Video length: {}".format(video_length))
			omxplayer = subprocess.Popen(['/usr/bin/omxplayer', '--loop', '--vol', '-6000', video_file], stdin=subprocess.PIPE, stdout=None, stderr=None, bufsize=0)
			time.sleep(delay)
			omxplayer.stdin.write(b'q')
	return True

def get_video_status():
	"""
	Returns status of video.
	Examples: playing, paused, file playing, etc.
	"""
	pass

def find_omxplayer():
	"""
	Gets PID of omxplayer.
	"""
	process = subprocess.call(['pgrep', 'omxplayer'])
	return process

def get_video_length(video_fullpath):
	"""
	Gets length of video.
	"""
	video_length = subprocess.run(
		['mediainfo', video_fullpath, '--Output="General;%Duration/String3%"', 'input'],
		stdout=subprocess.PIPE
	)
	print("video length: {}".format(video_length))
	return parse_video_length(video_length)

def parse_video_length(len_str):
	"""
	Parses video length string.
	Ex: 00:12:42.853 -> 762.853 seconds.
	"""
	# try:
	split_str = len_str.split(":")  # split by hr, min, sec
	if not len(split_str) == 3:
		return {'error': "incorrect size"}
	tot_secs = int(split_str[0])*3600 + int(split_str[1])*60 + float(split_str[2])
	print("total video length (s): {}".format(tot_secs))
	return tot_secs




if __name__ == '__main__':

	try:
		mode = sys.argv[1]  # 'folder' or 'file'
	except IndexError:
		print("First argument, 'mode', not provided. Defaulting to play_folder() with defaults.")
		play_folder()

	try:
		input2 = sys.argv[2]
	except IndexError:
		print("input2 not provided (filename or delay). Using default value.")

	try:
		folder = sys.argv[3]
	except IndexError:
		print("'folder' argument not provided. Using default value.")


	if mode == 'folder':
		delay = input2
		play_folder(delay, folder)

	elif mode == 'file':
		filename = input2
		play_video(filename, folder)
