from flask import Flask, Response, request, g
import os
import sys
import logging
import json
import time

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

sys.path.insert(1, PROJECT_ROOT)

# Local imports:
# import cli
from environment import load_env
from video_controls import video_api
from relay_controls import relay_api
#from skeleton import skeleton_api


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

load_env()

secret_key = os.urandom(24).hex()
os.environ.setdefault("SECRET_KEY", secret_key)

# Declares Flask application:
app = Flask(__name__)
app.config.update(
    DEBUG=True,
    SECRET_KEY=secret_key,  # set here as well as os.environ?
)

logging.basicConfig(
    level=logging.DEBUG
)  # sets logging level for logger (vary with dev vs prod?)

# Registered Blueprints:
app.register_blueprint(video_api.video_blueprint)
app.register_blueprint(relay_api.relay_blueprint)
#app.register_blueprint(skeleton_api.skeleton_blueprint)
# app.register_blueprint(cli.cli_blueprint)



if __name__ == "__main__":
    app.run('0.0.0.0', debug=True)
