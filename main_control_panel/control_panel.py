import sys
import os
import RPi.GPIO as GPIO
from time import sleep
# from threading import Thread

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

print(PROJECT_ROOT)

sys.path.insert(1, os.path.join(PROJECT_ROOT, ".."))

from control_module.control_module import ControlModule
#from spaceship_controls import environment
from environment import load_env





class SpaceshipController:

	def __init__(self):
		# Defines pins (BOARD, rewire post-CI):
		self.power_supply_output = 22  # turns main power supply on/off (green wire on PS)
		self.green_power_led = 1  # pin number for green power led (3.3V pin, not GPIO)
		self.red_flashing_led = 18  # pin number for red flashing led (main PS is on)
		self.push_button_led = 26  # led inside push button that starts main power supply
		self.start_power_button = 32  # push button that initiates turning on main power supply

		self.main_ps_on = False  # bool for if main power supply is on

		# GPIO PWM settings:
		self.red_flashing_led_pwm = None
		self.push_button_led_pwm = None

		# self.control_module = ControlModule()  # CI module instance

		load_env()

	def setup_gpio(self):
		"""
		Sets up RPi GPIO pins.
		"""
		GPIO.setmode(GPIO.BOARD)
		GPIO.setup(self.power_supply_output, GPIO.OUT)  # turns on/off power supply
		GPIO.setup(self.push_button_led, GPIO.OUT)  # led inside push button for power supply on/off
		GPIO.setup(self.start_power_button, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  # push button for power supply on/off
		# GPIO.setup(self.start_power_button, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # push button for power supply on/off
		GPIO.add_event_detect(self.start_power_button, GPIO.RISING, callback=self.handle_start_button, bouncetime=10000)
		GPIO.setup(self.red_flashing_led, GPIO.OUT)
		self.red_flashing_led_pwm = GPIO.PWM(self.red_flashing_led, 5)  # 5Hz PWM
		GPIO.setup(self.push_button_led, GPIO.OUT)
		self.push_button_led_pwm = GPIO.PWM(self.push_button_led, 5)  # 5Hz PWM

		# Sets up control interface GPIO:
		# self.control_module.setup_gpio()

	def cleanup(self):
		self.main_ps_on = False
		GPIO.cleanup()

	def handle_start_button(self, channel):
		"""
		Callback for when flashing push button that starts main power supply
		and 12V lines is pressed.
		"""
		print("Initial start push button value: {}".format(GPIO.input(channel)))
		sleep(0.01)
		if not GPIO.input(channel):
			return
		print("Post-sleep start push button value: {}".format(GPIO.input(channel)))
		start_btn_val = GPIO.input(self.start_power_button)
		if start_btn_val == True and self.main_ps_on:
			# button is pressed, and power supply is on:
			print("Main power supply button pressed while already on. Shutting power supply down.")
			self.handle_main_ps_power(False)  # turns main power supply on
		elif start_btn_val == True and not self.main_ps_on:
			# button is pressed, power supply is off:
			print("Main power supply button pressed. Turning on power supply.")
			self.handle_main_ps_power(True)

		self.flash_red_switch_button()
		self.handle_red_flashing_lights()

	def handle_red_flashing_lights(self):
		"""
		Flashes LED in push button that starts the
		main power supply and updates the on/off switch
		red led as well.
		"""
		if not self.main_ps_on:
			print("Starting push button flashing for PS start button")
			self.push_button_led_pwm.start(50)  # starts pb led pwm (DC 50%)
			return
		# self.push_button_led_pwm.stop()  # stops pb led pwm
		self.push_button_led_pwm.ChangeDutyCycle(100)
		self.flash_red_switch_button()  # starts flashing main on/off red led
		return

	def flash_red_switch_button(self):
		"""
		Flashes LED on the main on/off switch on the panel.
		"""
		print("Initiating main switch's red led to flash.")
		if self.main_ps_on:
			self.red_flashing_led_pwm.start(50)  # 50% DC
		else:
			self.red_flashing_led_pwm.stop()

	def handle_main_ps_power(self, turn_on=False):
		"""
		Turns on main power supply on or off,
		depending on what state it's in.
		"""
		if turn_on:
			print("Turning on power supply.")
			GPIO.output(self.power_supply_output, GPIO.HIGH)
			self.main_ps_on = True
		else:
			print("Turning off power supply.")
			GPIO.output(self.power_supply_output, GPIO.LOW)
			self.main_ps_on = False

	# def check_power_supply_button(self):
	# 	"""
	# 	Checks push button state that turns on power supply.
	# 	"""
	# 	start_ps_btn_pressed = GPIO.input(self.start_power_button)
	# 	if start_ps_btn_pressed == True and self.main_ps_on:
	# 		# button is pressed, and power supply is on:
	# 		print("Main power supply button pressed while already on. Shutting power supply down.")
	# 		self.handle_main_ps_power(False)  # turns main power supply on
	# 	elif start_ps_btn_pressed == True and not self.main_ps_on:
	# 		# button is pressed, power supply is off:
	# 		print("Main power supply button pressed. Turning on power supply.")
	# 		self.handle_main_ps_power(True)
	# 	# elif start_ps_btn_pressed = False and not self.main_ps_on:
	# 	# 	# button isn't pressed, power supply is off:
	# 	# 	pass  # do nothing
	# 	# elif start_ps_btn_pressed = False and self.main_ps_on:
	# 	# 	# button isn't pressed, power supply is on:
	# 	# 	pass  # do nothing
	# 	# sleep(0.2)

	def main(self):
		"""
		Executes main RPi code.
		"""
		# while True:
		# 	self.check_power_supply_button()
		self.setup_gpio()
		self.handle_red_flashing_lights()


if __name__ == '__main__':

	spaceship = SpaceshipController()

	try:

		print("Initiating spaceship controller.")
		print("Running spaceship controller main function.")

		spaceship.main()

		while True:
			sleep(0.01)

	except Exception as e:
		print("Exception occurred: {}".format(e))
		print("Shutting down spaceship controls.")
		spaceship.cleanup()
