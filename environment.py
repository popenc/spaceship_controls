import os
from dotenv import load_dotenv

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

print("environment.py PROJECT_ROOT: {}".format(PROJECT_ROOT))

def load_env():
	env_path = os.path.join(PROJECT_ROOT, '.env')
	load_dotenv(env_path)
	print("Loaded environment: {}".format(env_path))
