from flask import Flask, Response, jsonify, request, Blueprint
import os
import sys
import logging
import redis

# local imports:
from voice import skeleton_voice
from display.skeleton_crown import SkeletonCrown
from eyes.skeleton_eyes import SkeletonEyes

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(1, os.path.join(PROJECT_ROOT, ".."))

# Loads local environment:
from environment import load_env
load_env()

skeleton_blueprint = Blueprint("skeleton", __name__)

display = SkeletonCrown()
eyes = SkeletonEyes()



@skeleton_blueprint.route("/skeleton", methods=["GET"])
def skeleton_api():
    return jsonify([
    	"/skeleton/eyes/off",
		"/skeleton/eyes/on",
		"/skeleton/eyes/flash/<int:delay>",
		"/skeleton/crown/off",
		"/skeleton/crown/<string:message>",
		"/skeleton/crown/flash/<string:message>/<int:delay>",
		"/skeleton/speak"
	])

@skeleton_blueprint.route("/skeleton/eyes/off", methods=["GET"])
def turn_off_eyes(outlet_number):
	# Turns off led eyes.
	response = eyes.turn_off_eyes()
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/eyes/on", methods=["GET"])
def turn_on_eyes(outlet_number):
	# Turns on led eyes.
	response = eyes.turn_on_eyes()
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/eyes/flash/<int:delay>", methods=["GET"])
def flash_eyes(outlet_number):
	# Flash skeleton eyes.
	response = eyes.flash_eyes()
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/crown/off", methods=["GET"])
def turn_lcd_off():
	# Turns LCD off.
	response = display.clear_lcd()
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/crown/<string:message>", methods=["GET"])
def display_lcd(message="kill"):
	# Displays LCD message.
	response = display.flash_text(message, delay)
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/crown/flash/<string:message>/<int:delay>", methods=["GET"])
def flash_lcd(message="kill", delay=1):
	# Flashes LCD message.
	response = display.flash_text(message, delay)
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/crown/flash/stop", methods=["GET"])
def stop_lcd_flash():
	# Stops LCD flash.
	response = display.stop_flash()
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/crown/marquee/<string:message>/<int:speed>", methods=["GET"])
def marquee_lcd(message="kill", speed=1):
	# Message marquee for LCD.
	response = display.flash_text(message, speed)
	return jsonify(response)

@skeleton_blueprint.route("/skeleton/speak", methods=["GET", "POST"])
def skeleton_speak():
	if request.method == "GET":
		text = request.args.get("text")
		logging.warning("text: {}".format(text))
	elif request.method == "POST":
		req_data = request.get_json()
		text = req_data.get("text")
	skeleton_voice.main(text)
	return jsonify({"status": "spoke"})
