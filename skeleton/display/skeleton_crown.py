from RPLCD import CharLCD
import time
import RPi.GPIO as GPIO


class SkeletonCrown:

	def __init__(self):
		self.lcd_cols = 16  # number of cols
		self.lcd_rows = 2  # number of rows
		self.lcd = None  # lcd class instance
		self.flashing = False  # flashing bool
		self.delay = 1  # flash delay

		# pins:
		self.pin_rs = 37  # selects b/w instruction or data register
		self.pin_e = 35  # "enabling" pin
		self.pins_data = [33, 31, 29, 23]  # data pins

		self._setup()

		print("Crown display ready.")

	def _setup(self):
		# GPIO.setmode(GPIO.BOARD)  # is this needed?
		self.lcd = CharLCD(cols=self.lcd_cols, rows=self.lcd_rows, pin_rs=self.pin_rs,
					pin_e=self.pin_e, pins_data=self.pins_data, numbering_mode=GPIO.BOARD,
					backlight_enabled=True)

	def clear_screen(self):
		self.flashing = False
		self.lcd.clear()

	def write_to_lcd(self, message="kill"):
		"""
		Write to LCD in 4 bit mode.
		"""
		self.lcd.write_string(u"{}".format(message))
		return {"status": "lcd message updated."}

	def stop_flash(self):
		"""
		Stops flashing text.
		"""
		self.flashing = False
		print("Stopping skeleton eye flashing.")
		return {"status": "lcd flashing stopped."}

	def flash_text(self, message, delay):
		"""
		flashing loop for flashing text on LCD.
		"""
		self.flashing = True
		print("Starting crown message flashing.")
		while self.flashing:
			self.write_to_lcd(message)
			time.sleep(delay)
			self.lcd.clear()
			time.sleep(delay)
		return {"status": "lcd flashing stopped."}