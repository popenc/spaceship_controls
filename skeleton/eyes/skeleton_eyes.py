import time
import RPi.GPIO as GPIO


class SkeletonEyes:

	def __init__(self):
		self.flashing = False  # flashing bool
		self.delay = 1  # flash delay (s)
		self.delay_min = 0.05  # min delay (s)
		self.delay_max = 5  # max delay (s)

		# pins:
		self.pins_eyes = [3, 5]

		self._setup()

		print("Skeleton eyes ready.")

	def _setup(self):
		GPIO.setmode(GPIO.BOARD)
		for pin in self.pins_eyes:
			GPIO.setup(pin, GPIO.OUT)

	def _validate_delay(self, delay):
		"""
		"""
		if not (isinstance(delay, float) or isinstance(delay, int)):
			return False
		elif delay < self.delay_min or delay > self.delay_max:
			return False
		else:
			return True

	def _convert_to_number(self, delay):
		"""
		"""
		try:
			return float(delay)
		except Exception as e:
			print("_convert_to_number exception: {}".format(e))
			return False

	def turn_off_eyes(self):
		"""
		"""
		self.flashing = False
		for pin in self.pins_eyes:
			GPIO.output(PIN, GPIO.LOW)

	def turn_on_eyes(self):
		"""
		"""
		self.flashing = False
		for pin in self.pins_eyes:
			GPIO.output(PIN, GPIO.HIGH)

	def flash_eyes(self, delay=self.delay):
		"""
		"""
		self.flashing = True
		print("Starting skeleton eye flashing.")
		while self.flashing:
			self.turn_off_eyes()
			time.sleep(delay)
			self.turn_on_eyes()
			time.sleep(delay)
		return {"status": "skeleton eye flashing stopped."}

	def stop_flashing(self):
		"""
		"""
		self.flashing = False
		print("Stopped skeleton eye flashing.")
		return {"status": "skeleton eyes flashing stopped."}
