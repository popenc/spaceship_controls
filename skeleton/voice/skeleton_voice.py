import subprocess
import sys
import time
import os
import glob

orig_file = "voice.wav"
new_file = "voice-new.wav"

def filter_audio():
    subprocess.run([
        "sox", orig_file, new_file,
        "speed", "0.8",
        "pitch", "-220",
        "tremolo", "60", "40",
        "overdrive", "1",
        "reverb",
        "echo", "0.8", "0.88", "100", "0.4",
    ])

def remove_files():
    for f in glob.glob("voice*.wav"):
        os.remove(f)

def play_audio():
    subprocess.run(["aplay", new_file])

def handle_tts(text):
    subprocess.run(["pico2wave", "-w", orig_file, text])

def read_text_from_file():
    pass

def main(text):
    handle_tts(text)  # converts text to .wav file
    time.sleep(0.1)
    filter_audio()  # manipulates .wav file
    time.sleep(0.1)
    play_audio()  # plays new .wav file
    remove_files()  # removes files



if __name__ == '__main__':
    text = sys.argv[1]  # text user input
    main(text)
