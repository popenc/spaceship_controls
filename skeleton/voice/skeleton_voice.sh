#!/bin/sh

pico2wave -w input.wav $2
sox -r 22050 input.wav input-new.wav speed $1
sox -T input-new.wav sine.wav input-distored.wav
aplay input-distored.wav
rm input*.wav