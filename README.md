# spaceship_controls

**app.py** -- runs Flask API for the spaceship components.

**cli.py** -- command line interface for spacehip controls.

**main_control_panel** -- runs the main control code for the raspberry pi.

**arduino** -- folder for arduino sketches.

**control_module** -- 8 button controller primarily intended to control outlet relays.

**spaceship_videos** -- collection of videos for the monitors.

**video_controls** -- api and logic for playing videos.
