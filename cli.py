import sys
import os
import click
import subprocess
from flask import Blueprint

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

sys.path.insert(1, PROJECT_ROOT)

# Local imports:
import app
from video_controls import video_controller


cli_blueprint = Blueprint("cli", __name__)  # NOTE: Can bp name be blank?

# @click.group()
# def main():
#     """
#     Simple CLI for querying books on Google Books by Oyetoke Toby
#     """
#     pass

@cli_blueprint.cli.command('play')
@click.option('--video', '-v', help="Video filename to play.")
@click.option('--folder', '-f', help="Folder the video is located in.")
def play(video=None, folder=None):
    """
    Play a video file.
    """
    player_obj = video_controller.play_video(video, folder)
    click.echo("Started video '{}', pid: {}".format(video, player_obj.pid))

@cli_blueprint.cli.command('play_folder')
@click.option('--delay', '-d', is_flag=True, help="How long to play each video (will loop if finishes before delay is over).")
@click.option('--folder', '-f', is_flag=True, help="Folder the video is located in.")
def play_folder(delay=None, folder=None):
    click.echo("")
    player_obj = video_controller.play_folder(delay, folder)
    click.echo("Finished playing folder of videos.")

def stop():
    pass

@cli_blueprint.cli.command('stop_all')
def stop_all():
    """
    Stops all instances of omxplayer.
    """
    click.echo("Killing omxplayer processes.")
    subprocess.call(["killall", "omxplayer.bin"])
    click.echo("Killed omxplayer processes.")

def pause():
    pass

if __name__ == "__main__":
    main()
