#define outputA 6
#define outputB 7
int ledPin = 2; // choose the pin for the LED
int inPin = 3;   // choose the input pin (for a pushbutton)
int val = 0;     // variable for reading the pin status
int counter = 0;
int aState;
int aLastState;


void setup() {
  pinMode(ledPin, OUTPUT);  // declare LED as output
  pinMode(inPin, INPUT);    // declare pushbutton as input

  pinMode(outputA, INPUT);
  pinMode(outputB, INPUT);
  Serial.begin(9600);
  aLastState = digitalRead(outputA);
}


void loop(){
  
  // Push-button/LED test code:
  val = digitalRead(inPin);  // read input value
  if (val == HIGH) {         // check if the input is HIGH (button released)
    digitalWrite(ledPin, HIGH);  // turn LED OFF
  } else {
    digitalWrite(ledPin, LOW);  // turn LED ON
  }

  // Rotary encoder code:
  aState = digitalRead(outputA);
  if (aState != aLastState) {
    if (digitalRead(outputB) != aState) {
      counter++;
    }
    else {
      counter--;
    }
    Serial.print("Position: ");
    Serial.println(counter);
  }
  aLastState = aState;
  
}
