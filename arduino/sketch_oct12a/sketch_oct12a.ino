#include "Servo.h"

Servo myservo;

void setup() {
  myservo.attach(9);
}

void loop() {
  for(int x=0; x<180; x++){
    myservo.write(x);
    delay(50);
  }
  for(int x=180; x>0; x--){
    myservo.write(x);
    delay(50);
  }
}
