/*
 * Testing for the 8-outlet relay device.
 * TODO: Get a cover for it that's safe (i.e., not cardboard).
 */

const int relayPins[] = {2, 3, 4, 5, 6, 7, 8, 9};  // relay pins for each outlet
const int relayCount = 8;  // number of pins
char receivedChar;  // received character from serial monitor
boolean newData = false;  // used to indicate when to print to serial monitor
boolean outletStates[] = {false, false, false, false, false, false, false, false};  // false = off, true = on

void setup() {
  Serial.begin(9600);
  // Sets up relay pins as output pins:
  for (int i = 0; i < relayCount; i++) {
    Serial.print("Setting relay pin ");
    Serial.print(relayPins[i]);
    Serial.println(" to OUTPUT.");
    pinMode(relayPins[i], OUTPUT);
  }
  turnOutletsOff();
  Serial.println("<Arduino is ready>");
}

void loop() {
  delay(10);
  recvOneChar();
  showNewData();
}

void switchOutlet(char outletNumber) {
  int outletInt = outletNumber - '0';
  if (outletInt < 1 || outletInt > 8) {
    Serial.println("Enter a number between 1 and 8.");
    return;
  }
  if (outletStates[outletInt - 1] == false) {
    // Turns outlet on if state is false/off:
    digitalWrite(relayPins[outletInt - 1], LOW);
    outletStates[outletInt - 1] = true;
    Serial.print("Outlet ");
    Serial.print(outletNumber);
    Serial.println(" turned on.");
  }
  else {
    // Turns outlet off if state is true/on
    digitalWrite(relayPins[outletInt - 1], HIGH);
    outletStates[outletInt - 1] = false;
    Serial.print("Outlet ");
    Serial.print(outletNumber);
    Serial.println(" turned off.");
  }
  
}

void turnOutletsOff() {
  /*
   * Turns all 8 outlets off.
   */
  for (int i = 0; i < relayCount; i++) {
    Serial.print("Turning off outlet ");
    Serial.println(relayPins[i] - 1);
    digitalWrite(relayPins[i], HIGH);  // wired as normally closed
    outletStates[i] = false;
    delay(100);
  }
}

void recvOneChar() {
  if (Serial.available() > 0) {
    receivedChar = Serial.read();
    newData = true;
  }
}

void showNewData() {
  if (newData == true) {
    Serial.print("Received char: ");
    Serial.println(receivedChar);
    // Turns on outlet corresponding to received number (1 - 8):
    switchOutlet(receivedChar);
    newData = false;
  }
}

