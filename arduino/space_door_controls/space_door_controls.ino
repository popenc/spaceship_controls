/*
 * This is a combination of the "front_panel_blue_flash"
 * sketch and the "keypad_and_password" sketch. It handles the
 * main controls for the space door, like the keypad, buzzer, led
 * and/or push button, rf receiver, and servos (if hardware allows).
 */

#include <RH_ASK.h>
#include <SPI.h>

RH_ASK rf_driver;  // creates amplitude shift keying object

int flashDelay = 500;  // delay in ms between flashes
String openMessage = "open";  // expected message for opening door

void setup() {
  Serial.begin(9600);
  pinMode(initPin, INPUT);  // button that initiates door opening/closing
  rf_driver.init();
}

void loop() {
  delay(10);  // is this necessary?
  checkForRFMessage();
}

void checkForRFMessage() {
  uint8_t buf[4];
  uint8_t buflen = sizeof(buf);
  if (rf_driver.recv(buf, &buflen)) {
    Serial.print("Message received: ");
    receivedMessage = String((char*)buf);
    Serial.println(receivedMessage);
    if (receivedMessage.equals(openMessage))) {
      //executeOpeningAndClosing();  // that's what she said
      Serial.println("Opening door.");
    }
  }
}

//void sendMessageToOpenDoor(String message) {
//  static char *msg = message.c_str();
//  rf_driver.send((uint8_t *)msg, strlen(msg));
//  rf_driver.waitPacketSent();
//}
