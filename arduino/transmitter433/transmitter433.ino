#include <RH_ASK.h>  // radiohead amplitude shift keying lib
#include <SPI.h>  // SPI lib

RH_ASK rf_driver;  // amplitude shift keying object

//char openMsg[4] = "open";
String openMsg = "open";

int initPin = 8;  // button that initiates door opening

void setup() {
  
  rf_driver.init();

  pinMode(initPin, INPUT);
  
}

void loop() {

  delay(100);

  int pushBtnVal = digitalRead(initPin);

  if (pushBtnVal) {
    sendMessage(openMsg);
  }

}

void sendMessage(String message) {
  static char *msg = message.c_str();
  rf_driver.send((uint8_t *)msg, strlen(msg));
  rf_driver.waitPacketSent();
}

