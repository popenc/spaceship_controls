/*
Testing the IR transmitter from the "37 sensor kit v2.0".
The purpose is to be able to turn on/off the spaceship "monitors"
(they're TVs), and probably be able to change their input and volume
as well.
*/

#include <IRremote.h>

// Insignia NS-RC4NA-14 codes for main control panel monitor:
#define KEY_POWER "61A0F00F"
#define KEY_INPUT "61A0B847"
#define KEY_VOL_UP "61A030CF"
#define KEY_VOL_DWN "61A0B04F"
#define KEY_MUTE "61A0708F"

IRsend irsend;

void setup ()
{
	Serial.begin(9600);
}

void loop () {
	irsend.sendNEC(KEY_POWER, 32);  // Insignia TV, sends 32bit NEC code
	delay(5000);
}