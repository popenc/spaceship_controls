#include <Keypad.h>

#define passwordLength 4  // 3 chars + NULL char

// Password settings:
char enteredPassword[passwordLength];  // var for entered password
char actualPassword[passwordLength] = "666";  // the actual password
byte dataCount = 0, masterCount = 0;

// Buzzer settings:
int buzzerPin = 13;

// LED settings:
int ledPin = 11;

// Motion detector settings:
int pirPin = 12;
int pirVal;

// LED "breathing" settings:
//int led0 = 10;  // pwm pin 0
//int led1 = 11;  // pwm pin 1
int brightness = 0;  // led brightness, from 0 to 255
int fadeAmount = 5;  // how many points to fade leds by

// Keypad settings:
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3', 'A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {9, 8, 7, 6}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {5, 4, 3, 2}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

void setup(){
  Serial.begin(9600);
  pinMode(buzzerPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(pirPin, INPUT);
//  pinMode(led0, OUTPUT);
//  pinMode(led1, OUTPUT);
}
  
void loop(){
  
  char customKey = customKeypad.getKey();  // gets pressed key value

  // Adds key to array if its correct and in sequence:
  if (customKey && customKey == actualPassword[dataCount]){
    enteredPassword[dataCount] = customKey;  // stores char into array
    dataCount++;
  }
  // Clears password array and dataCount if next key is out of sequence:
  else if (customKey && customKey != actualPassword[dataCount]) {
    resetPasswordVars();
  }

  if (dataCount == passwordLength - 1) {
    if (!strcmp(enteredPassword, actualPassword)) {
      // Password is correct!
      initiatePasswordSuccessRoutine();
      resetPasswordVars();
    }
  }


  startLedBreathing();
  

  // Reads motion detector data:
  pirVal = digitalRead(pirPin);
//  Serial.print("PIR: ");
//  Serial.println(pirVal);

//  Serial.print("Pin 11: ");
//  Serial.println(analogRead("A0"));
  
//  if (pirVal == 1) {
//    // Motion detected, starts LED "breathing" (fade in and out):
////    Serial.println("Starting LED breathing.");
////    startLedBreathing();
////    analogWrite(led0, 254);
////    analogWrite(led1, 255);
//  }
//  else {
//    // No motion, keeps leds off:
////    stopLedBreathing();
//  }
  
}

void startLedBreathing() {
  Serial.print("Brightness: ");
  Serial.println(brightness);
//  analogWrite(led0, brightness);
//  analogWrite(led1, brightness);
  analogWrite(ledPin, brightness);
//  analogWrite(led2, brightness);
//  analogWrite(led3, brightness);
  brightness = brightness + fadeAmount;
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  delay(30);  // needed? use millis in main loop?
}

void stopLedBreathing() {
  brightness = 0;
  analogWrite(ledPin, brightness);
//  analogWrite(led0, brightness);
//  analogWrite(led1, brightness);
//  analogWrite(led2, brightness);
//  analogWrite(led3, brightness);
}

void initiatePasswordSuccessRoutine() {
  digitalWrite(buzzerPin, HIGH);
//  digitalWrite(ledPin, HIGH);
  delay(1000);  // TODO: use millis(), and flash LED
  digitalWrite(buzzerPin, LOW);
//  digitalWrite(ledPin, LOW);
}

void resetPasswordVars() {
  memset(enteredPassword, 0, sizeof(enteredPassword));  // resets char array
  dataCount = 0;
}

