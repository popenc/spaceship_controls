/*
 * This is a combination of the "front_panel_blue_flash"
 * sketch and the "keypad_and_password" sketch. It handles the
 * main control panel features, like turning on/off lights manually
 * with switches/buttons, blinking lights (settings, knobs), sending RF
 * messages (e.g., opening door from control panel), turning on fog
 * eventually.
 */

#include <RH_ASK.h>
#include <SPI.h>

RH_ASK rf_driver;  // creates amplitude shift keying object

int ledArray[] = {7,6,5,4,3,2};  // pin numbers for led array (top to bottom)
int pinCount = 6;
int numLeds = 0;
int flashDelay = 500;  // delay in ms between flashes
bool ledBlinkState = HIGH;
long ledTimer;
int potPin = 0;
int ledPin1 = 2;
int initPin = 8;

float slope = -1.39;
float intercept = 1475.23;

String openMessage = "open";

void setup() {

  numLeds = sizeof(ledArray);

  Serial.begin(9600);

  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    pinMode(ledArray[thisPin], OUTPUT);
  }

  pinMode(initPin, INPUT);  // button that initiates door opening/closing

  rf_driver.init();
  
}

void loop() {
  
  // Loops from bottom to top:
  for (int thisPin = 0; thisPin < pinCount; thisPin++) {
    ledTimer = millis();    
    digitalWrite(ledArray[thisPin], HIGH);
    while (millis() - ledTimer < flashDelay) {

      // Check knob value:
      int potVal = analogRead(potPin);
      flashDelay = determineFlashFrequency(potVal);

      // Check button value:
      int pushBtnVal = digitalRead(initPin);
      if (pushBtnVal) {
        sendMessageToOpenDoor(openMessage);
      }
      
    }
    digitalWrite(ledArray[thisPin], LOW);  
  }

  // Loops from top to bottom:
  for (int thisPin = pinCount - 1; thisPin >= 0; thisPin--) {
    ledTimer = millis();    
    digitalWrite(ledArray[thisPin], HIGH);
    while (millis() - ledTimer < flashDelay) {
      int potVal = analogRead(potPin);
      flashDelay = determineFlashFrequency(potVal); 
    }
    digitalWrite(ledArray[thisPin], LOW);
  }
  
}

void sendMessageToOpenDoor(String message) {
  static char *msg = message.c_str();
  rf_driver.send((uint8_t *)msg, strlen(msg));
  rf_driver.waitPacketSent();
}

float determineFlashFrequency(float currPotVal) {
  return slope*currPotVal + intercept;
}
