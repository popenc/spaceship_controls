#include <Servo.h>

Servo myservo9;
Servo myservo6;
Servo myservo5;
Servo myservo3;

int delayTime = 2000;

long mainLoopTimer;

int slowCCW = 80;
int slowCW = 100;
int stopVal = 90;

int initPin = 2;  // pin that starts sliding

bool isMoving = false;  // true if door is opening or closing

String receivedMessage;

void setup() {
  Serial.begin(9600);
  myservo9.attach(9);
  myservo6.attach(6);
  myservo5.attach(5);
  myservo3.attach(3);
  pinMode(initPin, INPUT);
}

void loop() {
  mainLoopTimer = millis();
  while (millis() - mainLoopTimer < 100) {
    int openPinVal = digitalRead(initPin);
    Serial.print("open pin val: ");
    Serial.println(openPinVal); 
    if (openPinVal) {
      executeOpeningAndClosing(); 
    }
    else {
      stopSliding();
    }
  }
}

void executeOpeningAndClosing () {
  isMoving = true;
  myservo9.write(slowCCW);
  myservo6.write(slowCW);
  myservo5.write(slowCW);
  myservo3.write(slowCCW);
  delay(delayTime);
  myservo9.write(slowCW);
  myservo6.write(slowCCW);
  myservo5.write(slowCCW);
  myservo3.write(slowCW);
  delay(delayTime);
  stopSliding();
  isMoving = false;
}

void stopSliding() {
  myservo9.write(stopVal);
  myservo6.write(stopVal);
  myservo5.write(stopVal);
  myservo3.write(stopVal);
}

