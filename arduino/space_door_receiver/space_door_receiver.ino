#include <RH_ASK.h>  // radiohead amplitude shift keying lib
#include <SPI.h>  // SPI lib

RH_ASK rf_driver;  // amplitude shift keying object

String receivedMessage;

int doorPin = 2;  // output pin that tells space door servo arduino to open/close door

void setup() {
  rf_driver.init();
  Serial.begin(9600);
  pinMode(doorPin, OUTPUT);
}

void loop() {
  delay(10);
  checkForRFMessage();
}

void checkForRFMessage() {
  uint8_t buf[4];
  uint8_t buflen = sizeof(buf);
  if (rf_driver.recv(buf, &buflen)) {
    Serial.print("Message received: ");
    receivedMessage = String((char*)buf);
    Serial.println(receivedMessage);
  }
}
